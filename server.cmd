@echo off
REM Web Application Design - Web Server Launcher v2.0 Summer 2014
TITLE Web Site

ver | findstr /i "5\.1\." > nul
IF %ERRORLEVEL% EQU 0 goto ver_XP
ver | findstr /i "6\.1\." > nul
IF %ERRORLEVEL% EQU 0 goto ver_Win7
goto warn_and_exit

:ver_Win7
:Run Windows 7 specific commands here
REM echo OS Version: Windows 7 (debug line)
"C:\Program Files (x86)\IIS Express\iisexpress.exe" /port:3000 /path:"C:\Temp\%USERNAME%\katzJS\angular"
goto end

:ver_XP
:Run Windows XP specific commands here
REM echo OS Version: Windows XP (debug line)
"C:\Program Files\IIS Express\iisexpress.exe" /port:3000 /path:"C:\Temp\%USERNAME%\katzJS\angular"
goto end

:warn_and_exit
echo Machine OS cannot be determined.

:end 