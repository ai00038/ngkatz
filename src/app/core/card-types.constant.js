(function () {
    'use strict';

	var BASE_PATH = 'src/content/images/cards/',
		POWERLESS_DESC = 'This card is powerless on its own, but can be played in pairs or special combos';
	
    angular
        .module('core')
		.constant('katzCardTypes', {
			POWERLESS_DESC:POWERLESS_DESC,
			explodingKitten: {
				name: 'Exploding Kitten',
				description: 'If you draw this card, show it immediately. Then, Unless you have a defuse card, you are dead. Then discard all of your cards including the exploding kitten',
				url: BASE_PATH + 'exploding-kitten.png'
			},
			defuse: {
				name: 'Defuse',
				description: 'If you drew an exploding kitten, you can play this card instead of dying. Then, take the exploding kitten, and place it anywhere you like in the deck.',
				url: BASE_PATH + 'defuse.png'
			},
			nope : {
				name: 'Nope',
				description: 'Stop any action except an Exploding Kitten or defuse.',
				url: BASE_PATH + 'nope.png'
			},
			attack : {
				name: 'Attack',
				description: 'End your turn without drawing, then force the next player to take the next two turns in a row.',
				url: BASE_PATH + 'attack.png'
			},
			skip : {
				name: 'Skip',
				description: 'Immediately end the your current turn without drawing a card.',
				url: BASE_PATH + 'skip.png'
			},
			favour : {
				name: 'Favour',
				description: 'Force any other player to give you one card from their hand. They choose which card to give you',
				url: BASE_PATH + 'favour.png'
			},
			shuffle : {
				name: 'Shuffle',
				description: 'The deck is shuffled',
				url: BASE_PATH + 'shuffle.png'
			},
			seeFuture : {
				name: 'See the Future',
				description: 'Look at the top three cards of the deck',
				url: BASE_PATH + 'see-the-future.png'
			},
			powerless1 : {
				name: 'Tacocat',
				description: POWERLESS_DESC,
				url: BASE_PATH + 'powerless1.png'
			},
			powerless2 : {
				name: 'Meloncat',
				description: POWERLESS_DESC,
				url: BASE_PATH + 'powerless2.png'
			},
			powerless3 : {
				name: 'Beancat',
				description: POWERLESS_DESC,
				url: BASE_PATH + 'powerless3.png'
			},
			powerless4 : {
				name: 'Beardcat',
				description: POWERLESS_DESC,
				url: BASE_PATH + 'powerless4.png'
			},
			powerless5 : {
				name: 'Rainbowcat',
				description: POWERLESS_DESC,
				url: BASE_PATH + 'powerless5.png'
			}
		});
		
})();