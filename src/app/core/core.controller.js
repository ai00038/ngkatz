(function () {
    'use strict';

    angular
        .module('core')
		.controller('CoreController', CoreController);
		
	function CoreController($timeout, $uibModal, katzCardTypes) {
		var vm = this,
		_discardPile = [],
		_deck = [],
		_players = [],
		_turnPlayer = 0;
		
		vm.messageBox = '';
		vm.hoveredCard = null;
		
		vm.toggleSelectCard = toggleSelectCard;
		vm.dealToPlayer = dealToPlayer;
		vm.playCards = playCards;
		vm.stop = stop;
		vm.getHandSize = getHandSize;
		vm.getYourHand = getYourHand;
		vm.viewDiscardPile = viewDiscardPile;
		vm.discardLength = discardLength;
		vm.deckLength = deckLength;
		vm.activate = activate;
		
		activate();

		function activate() {
			vm.loading = true;
			setupDefaults();
			setupDeckAndHands();
			vm.loading = false;
			startGameLoop();
		}
		
		function stop() {
			_.set(_players[0], 'isAlive', false);
			var modalInstance = createMessageModal('You have quit the game. Click New Game to play Again');
		}
		
		function playCards(cardsArr) {
			
			if (cardsArr.length === 1) {
				invokeIndiv(cardsArr[0]);
				finishPlayingAndDiscard();
			} else if (cardsArr.length === 2 && (cardsArr[0].name === cardsArr[1].name) ) {
				favour('Two of a Kind');
				finishPlayingAndDiscard();
			} else if (cardsArr.length === 3 && ( (cardsArr[0].name === cardsArr[1].name) && (cardsArr[0].name === cardsArr[2].name) ) ) {
				invokeThreeOfAKind();
				finishPlayingAndDiscard();
			} else if (cardsArr.length === 5 && R.allUniq(_.pluck(cardsArr, 'name'))) {
				reviveDiscard();
				finishPlayingAndDiscard();
			} else {
				createMessageModal('Invalid Selection of Cards. The following selections, however are valid: One card on its own, Two of the same card, Three of the same card and five different Cards.');
			}
		
			function finishPlayingAndDiscard() {
				_.forEach(cardsArr, function (card) {
					_discardPile.push(card);
					_.set(card, '_selected', false);
					_.remove(_players[0].hand, function (_card) {
						return _.isEqual(card, _card);
					});
				});
			}
		}
		
		function invokeIndiv(card) {
			switch(card.name) {
				case 'Shuffle':
					doShuffle();
					break;
				case 'See the Future':
					seeTheFuture();
					break;
				case 'Favour':
					favour('Favour');
					break;
				case 'Attack':
					attack();
					break;
				case 'Skip':
					skip();
					break;
				default:
					console.log('card is powerless');
			}
		}
		
		function invokeThreeOfAKind() {
			var modalInstance = $uibModal.open({
			  templateUrl: 'src/app/core/partials/snatch-named.html',
			  controller: 'SnatchNamedController',
			  controllerAs: 'vm',
			  resolve: {
				opponents: _.constant(_.filter(_players, 'isYou', false))
			  }
			});
			
			modalInstance.result.then(function (oppCard){
				if(oppCard){
					var theCard = _.find(oppCard.opp.hand, 'name', oppCard.card.name);
					_players[0].hand.push(theCard);
					oppCard.opp.hand.splice(_.indexOf(theCard), 1);
					createMessageModal('You have taken ' + oppCard.card.name + ' from '+ oppCard.opp.name);
				} else {
					createMessageModal('Your opponent doesn\'t have that card');
				}
			}, function () {
			  console.log('Houston we have a problem');
			});
			// if they have that card, they give it to you, otherwise you get nothing
		}
		
		function reviveDiscard() {
			var modalInstance = $uibModal.open({
			  templateUrl: 'src/app/core/partials/revive-discard.html',
			  controller: 'ReviveDiscardController',
			  controllerAs: 'vm',
			  resolve: {
				discardPile: _.constant(_discardPile)
			  }
			});
			
			modalInstance.result.then(function (revivedCard) {
				_players[0].hand.push(revivedCard);
				_discardPile.splice(_.indexOf(revivedCard),1);
				createMessageModal('You have revived ' + revivedCard.name);
			}, function () {
			  console.log('Houston we have a problem');
			});
		}
		
		function doShuffle() {
			_deck = _.shuffle(_deck);
			var modalInstance = $uibModal.open({
			  templateUrl: 'src/app/core/partials/shuffle.html',
			  controller: 'MessageController',
			  controllerAs: 'vm',
			  resolve: {
				message: null
			  }
			});
		}
		
		function seeTheFuture() {
			var firstThree = _.take(_deck, 3);
			var modalInstance = $uibModal.open({
			  templateUrl: 'src/app/core/partials/see-the-future.html',
			  controller: 'SeeTheFutureController',
			  controllerAs: 'vm',
			  resolve: {
				firstThree: _.constant(firstThree)
			  }
			});
		}
		
		function favour(title) {
			var modalInstance = $uibModal.open({
			  templateUrl: 'src/app/core/partials/favour.html',
			  controller: 'FavourController',
			  controllerAs: 'vm',
			  resolve: {
				title: _.constant(title),
				opponents: _.constant(_.filter(_players, 'isYou', false)),
				giftedCard: _.constant(null),
				favourOpp: _.constant(null)
			  }
			}),
			opp;
			modalInstance.result.then(function (selectedOpp) {
			  opp = selectedOpp;
			  giftCard(selectedOpp);
			}, function () {
			  console.log('Houston we have a problem');
			});
			
			function giftCard(theOpp) {
				var index = _.random(0, theOpp.hand.length);
				var giftedCard = theOpp.hand[index];
				_players[0].hand.push(giftedCard);
				theOpp.hand.splice(index,1);
				modalInstance = $uibModal.open({
				  templateUrl: 'src/app/core/partials/favour.html',
				  controller: 'FavourController',
				  controllerAs: 'vm',
				  resolve: {
					title: _.constant(title),
					opponents: _.constant(null),
					giftedCard: _.constant(giftedCard),
					favourOpp: _.constant(theOpp)
				  }
				});
			}
			
		}
		
		function attack() {
		}
		
		function skip() {
		}
		
		function toggleSelectCard(card) {
			_.set(card, '_selected', !_.get(card, '_selected'));
		}
		
		function startGameLoop() {
			var modalInstance = createMessageModal('A New Game Has Started');
		}
		
		function createMessageModal(theMessage) {
			return $uibModal.open({
			  templateUrl: 'src/app/core/partials/message.html',
			  controller: 'MessageController',
			  controllerAs: 'vm',
			  resolve: {
				message: _.constant(theMessage)
			  }
			});
		}
		
		function viewDiscardPile() {
			var modalInstance = $uibModal.open({
			  templateUrl: 'src/app/core/partials/discard-pile.html',
			  controller: 'DiscardPileController',
			  controllerAs: 'vm',
			  resolve: {
				discardPile: _.constant(_discardPile)
			  }
			});
		}
		
		function setupDefaults() {
			_discardPile = [];
			_deck = [];
			_players = [
				{
					hand: [],
					name: 'You',
					isYou: true,
					isAlive: true
				},
				{
					hand: [],
					name: 'AI Player 1',
					isYou: false,
					isAlive: true
				},
				{
					hand: [],
					name: 'AI Player 2',
					isYou: false,
					isAlive: true
				},
				{
					hand: [],
					name: 'AI Player 3',
					isYou: false,
					isAlive: true
				},
				{
					hand: [],
					name: 'AI Player 4',
					isYou: false,
					isAlive: true
				}
			];
			_turnPlayer = 0;
		}
		
		function setupDeckAndHands() {
			
			addToDeck(5, katzCardTypes.nope);
			addToDeck(4, katzCardTypes.attack);
			addToDeck(4, katzCardTypes.skip);
			addToDeck(4, katzCardTypes.favour);
			addToDeck(4, katzCardTypes.shuffle);
			addToDeck(5, katzCardTypes.seeFuture);
			addToDeck(4, katzCardTypes.powerless1);
			addToDeck(4, katzCardTypes.powerless2);
			addToDeck(4, katzCardTypes.powerless3);
			addToDeck(4, katzCardTypes.powerless4);
			addToDeck(4, katzCardTypes.powerless5);
			
			_deck = _.shuffle(_deck);
			
			for(var i = 0; i < _players.length; i++){
				for(var j = 0; j < 4; j++){
					dealToPlayer(i);
				}
			}
			
			for(var i = 0; i < _players.length; i++){
				_players[i].hand.push(angular.copy(katzCardTypes.defuse));
			}
			
			addToDeck(6 - _players.length, katzCardTypes.defuse);
			addToDeck(_players.length - 1, katzCardTypes.explodingKitten);
			
			_deck = _.shuffle(_deck);
		}
		
		function dealToPlayer(index) {
			_players[index].hand.push(_deck[0]);
			_deck = _.rest(_deck);
		}
		
		function addToDeck(howMany, cardType) {
			for(var i = 0; i < howMany; i++){
				_deck.push(angular.copy(cardType));
			}
		}
		
		function deckLength() {
			return _deck.length;
		}
		
		function discardLength() {
			return _discardPile.length;
		}
		
		function getHandSize(index) {
			return _players[index].hand.length;
		}
		
		function getYourHand() {
			return _players[0].hand;
		}
	}
		
})();