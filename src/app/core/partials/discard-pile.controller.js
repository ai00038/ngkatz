(function () {
    'use strict';

    angular
        .module('core')
		.controller('DiscardPileController', DiscardPileController);
		
	function DiscardPileController($uibModalInstance, discardPile) {
		var vm = this;
		vm.discardPile = discardPile;
		vm.close = $uibModalInstance.close;
	}
		
})();