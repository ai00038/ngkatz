(function () {
    'use strict';

    angular
        .module('core')
		.controller('SnatchNamedController', SnatchNamedController);
		
	function SnatchNamedController($uibModalInstance, opponents, katzCardTypes) {
		var vm = this;
		vm.allCards = katzCardTypes;
		vm.opponents = opponents
		vm.nameOpponentAndCard = nameOpponentAndCard;
		
		function nameOpponentAndCard(opp, card) {
		
			if(_.includes(_.pluck(opp.hand, 'name'), card.name)) {
				$uibModalInstance.close({
					opp: opp,
					card: card
				});
			} else {
				$uibModalInstance.close(null);
			}
		}
		
	}
		
})();