(function () {
    'use strict';

    angular
        .module('core')
		.controller('MessageController', MessageController);
		
	function MessageController($uibModalInstance, message) {
		var vm = this;
		vm.message = message;
		vm.close = $uibModalInstance.close;
	}
		
})();