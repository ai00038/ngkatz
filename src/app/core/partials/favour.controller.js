(function () {
    'use strict';

    angular
        .module('core')
		.controller('FavourController', FavourController);
		
	function FavourController($uibModalInstance, title, opponents, giftedCard, favourOpp) {
		var vm = this;
		vm.title = title;
		vm.opponents = opponents;
		vm.giftedCard = giftedCard;
		vm.favourOpp = favourOpp;
		
		vm.selectOpp = selectOpp;
		vm.close = $uibModalInstance.close;
		
		// slight bug here, all cards used to invoke this ability get placed into discard pile before invokation. revival should happen BEFORE discard.
		
		function selectOpp(opp) {
			$uibModalInstance.close(opp);
		}
	}
		
})();