(function () {
    'use strict';

    angular
        .module('core')
		.controller('ReviveDiscardController', ReviveDiscardController);
		
	function ReviveDiscardController($uibModalInstance, discardPile) {
		var vm = this;
		vm.discardPile = discardPile;
		vm.takeCard = takeCard;
		
		function takeCard(card) {
			$uibModalInstance.close(card);
		}
	}
		
})();