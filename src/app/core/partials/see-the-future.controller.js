(function () {
    'use strict';

    angular
        .module('core')
		.controller('SeeTheFutureController', SeeTheFutureController);
		
	function SeeTheFutureController($uibModalInstance, firstThree) {
		var vm = this;
		vm.firstThree = firstThree;
		vm.close = $uibModalInstance.close;
	}
		
})();